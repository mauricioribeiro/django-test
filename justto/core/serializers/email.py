
from rest_framework import serializers

from ..models.email import *
from ..models.audit import *


class EmailSerializer(serializers.ModelSerializer):

    class Meta:
        model = Email
        fields = (
            'id',
            'address',
            'status'
        ) + Audit.fields()

