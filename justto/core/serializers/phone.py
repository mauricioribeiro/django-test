
from rest_framework import serializers

from ..models.phone import *
from ..models.audit import *


class PhoneSerializer(serializers.ModelSerializer):

    class Meta:
        model = Phone
        fields = (
            'id',
            'number',
            'status',
            'type'
        ) + Audit.fields()

