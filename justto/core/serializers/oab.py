
from rest_framework import serializers

from ..models.oab import *
from ..models.audit import *


class OabSerializer(serializers.ModelSerializer):

    class Meta:
        model = Oab
        fields = (
            'id',
            'state',
            'number',
            'status'
        ) + Audit.fields()

