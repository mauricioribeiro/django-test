from rest_framework import serializers

from ..serializers.oab import OabSerializer
from ..serializers.phone import PhoneSerializer
from ..serializers.email import EmailSerializer
from ..models.user import *
from ..models.email import *
from ..models.phone import *
from ..models.oab import *
from ..models.audit import *


class UserSerializer(serializers.ModelSerializer):
    customFields = serializers.HStoreField()
    emails = EmailSerializer(many=True)
    phones = PhoneSerializer(many=True)
    oabs = OabSerializer(many=True)

    class Meta:
        model = User
        fields = (
            'id',
            'person',
            'name',
            'legalId',
            'username',
            'customFields',
            'emails',
            'phones',
            'oabs'
        ) + Audit.fields()

    def create(self, validated_data):
        emails_data = validated_data.pop('emails')
        phones_data = validated_data.pop('phones')
        oabs_data = validated_data.pop('oabs')

        user = User.objects.create(**validated_data)

        for email_data in emails_data:
            Email.objects.create(user=user, **email_data)

        for phone_data in phones_data:
            Phone.objects.create(user=user, **phone_data)

        for oab_data in oabs_data:
            Oab.objects.create(user=user, **oab_data)

        return user

    def update(self, instance, validated_data):
        emails_data = validated_data.pop('emails')
        phones_data = validated_data.pop('phones')
        oabs_data = validated_data.pop('oabs')

        instance.person = validated_data.get('person', instance.person)
        instance.name = validated_data.get('name', instance.name)
        instance.legalId = validated_data.get('legalId', instance.legalId)
        instance.username = validated_data.get('username', instance.username)
        instance.is_admin = validated_data.get('is_admin', instance.is_admin)
        instance.customFields = validated_data.get('customFields', instance.customFields)
        instance.save()

        for email_data in emails_data:
            Email.objects.update_or_create(id=email_data['id'] if 'id' in email_data else None,
                                           user_id=instance.id,
                                           defaults=email_data)

        for phone_data in phones_data:
            Phone.objects.update_or_create(id=phone_data['id'] if 'id' in phone_data else None,
                                           user_id=instance.id,
                                           defaults=phone_data)

        for oab_data in oabs_data:
            Oab.objects.update_or_create(id=oab_data['id'] if 'id' in oab_data else None,
                                         user_id=instance.id,
                                         defaults=oab_data)

        return instance


