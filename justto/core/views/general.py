from httplib import OK

from rest_framework.generics import RetrieveAPIView
from rest_framework.response import Response


class RootView(RetrieveAPIView):

    def retrieve(self, request, *args, **kwargs):
        return Response(None, status=OK)

