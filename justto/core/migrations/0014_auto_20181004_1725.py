# -*- coding: utf-8 -*-
# Generated by Django 1.11.15 on 2018-10-04 17:25
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0013_oab'),
    ]

    operations = [
        migrations.AddField(
            model_name='email',
            name='user',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='emails', to=settings.AUTH_USER_MODEL, verbose_name=b'Usu\xc3\xa1rio'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='oab',
            name='user',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='oabs', to=settings.AUTH_USER_MODEL, verbose_name=b'Usu\xc3\xa1rio'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='phone',
            name='user',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='phones', to=settings.AUTH_USER_MODEL, verbose_name=b'Usu\xc3\xa1rio'),
            preserve_default=False,
        ),
    ]
