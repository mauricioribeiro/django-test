# coding=utf-8
from django.core.validators import RegexValidator
from django.db import models

from .user import User
from .utils import Status
from .audit import *


class Oab(Audit):
    user = models.ForeignKey(User, verbose_name='Usuário', on_delete=models.CASCADE, related_name='oabs')
    state = models.CharField(verbose_name='Estado', validators=[RegexValidator(regex='^[A-Z]{2}$')], max_length=2)
    number = models.CharField(verbose_name='Número', null=True, blank=True, max_length=25)
    status = models.CharField(verbose_name='Status', choices=Status.STATUSES, default=Status.ACTIVE[0], max_length=25)

    class Meta:
        verbose_name = 'Contato'
        ordering = ['id']

    def __str__(self):
        return '%s/%s' % (self.number, self.state) if self.number and self.state else None
