# coding=utf-8
from django.db import models

from .utils import Status
from .user import *
from .audit import *


class Email(Audit):
    user = models.ForeignKey(User, verbose_name='Usuário', on_delete=models.CASCADE, related_name='emails')
    address = models.CharField(verbose_name='Email', null=True, blank=True, max_length=100)
    status = models.CharField(verbose_name='Status', choices=Status.STATUSES, default=Status.ACTIVE[0], max_length=25)

    class Meta:
        verbose_name = 'Email'
        ordering = ['id']

    def __str__(self):
        return self.address
