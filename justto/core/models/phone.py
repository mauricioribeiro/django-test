# coding=utf-8
from django.db import models

from .utils import Status
from .audit import *
from .user import *


class PhoneType:
    WHATSAPP = ('WHATSAPP', 'Whatsapp')
    MOBILE = ('MOBILE', 'Celular')
    FIXED = ('FIXED', 'Telefone')

    TYPES = [
        WHATSAPP, MOBILE, FIXED
    ]

    def __init__(self):
        pass


class Phone(Audit):
    user = models.ForeignKey(User, verbose_name='Usuário', on_delete=models.CASCADE, related_name='phones')
    number = models.CharField(verbose_name='Número', null=True, blank=True, max_length=25)
    status = models.CharField(verbose_name='Status', choices=Status.STATUSES, default=Status.ACTIVE[0], max_length=25)
    type = models.CharField(verbose_name='Tipo', choices=PhoneType.TYPES, default=PhoneType.MOBILE[0], max_length=25)

    class Meta:
        verbose_name = 'Contato'
        ordering = ['id']

    def __str__(self):
        return self.number
