#!/usr/bin/env bash
export PYTHONPATH=/home/bitnami/django-test/justto/

NEWRELIC="newrelic"
DATADOG="datadog"

PORT=$2
WORKERS=$3

if [ -z "$1" ]
then
	echo "Tracer is empty. Options: $DATADOG or $NEWRELIC"
	exit
fi

if [ -z "$2" ]
then
	echo "Port not defined. Using default port (8080)"
	PORT="8080"
fi

if [ -z "$3" ]
then
        echo "Workers not defined. Using default workers (3)"
        WORKERS="3"
fi


if [ "$1" = "$NEWRELIC" ]
then
	NEW_RELIC_CONFIG_FILE=newrelic.ini newrelic-admin run-program gunicorn --bind 0.0.0.0:$PORT --workers $WORKERS justto.wsgi:application
fi

if [ "$1" = "$DATADOG" ]
then
	ddtrace-run gunicorn --bind 0.0.0.0:$PORT --workers=$WORKERS justto.wsgi:application
fi


